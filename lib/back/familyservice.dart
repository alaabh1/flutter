import 'dart:async';

import 'package:flutter_project/back/dbcreation.dart';

import 'package:sqflite/sqflite.dart';

import 'package:flutter_project/models/familymodel.dart';

class Familyservice {
  Future<void> insertfamille(Family famille) async {
    Database db = await Stockdatabase.getDatabase();

    await db.insert(
      'FAMILY',
      famille.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    print(famille.toMap());
  }

  static Future<List<Family>> getAllFamily() async {
    Database db = await Stockdatabase.getDatabase();
    List<Map<String, Object?>> mapFamily = await db.query("FAMILY");
    List<Family> allFamily = [];
    mapFamily.forEach((element) => allFamily.add(Family.fromMap(element)));
    return allFamily;
  }
}

