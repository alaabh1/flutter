import 'dart:async';

import 'package:flutter_project/back/dbcreation.dart';

import 'package:sqflite/sqflite.dart';

import 'package:flutter_project/models/componentsmodel.dart';

class Componentsservice {
  Future<void> insertcomponent(Components composant) async {
    Database db = await Stockdatabase.getDatabase();

    await db.insert(
      'COMPONENTS',
      composant.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    print(composant.toMap());
  }

  static Future<List<Components>> getAllcomponent() async {
    Database db = await Stockdatabase.getDatabase();
    List<Map<String, Object?>> mapComponent = await db.query("COMPONENTS");
    List<Components> allcomponent = [];
    mapComponent
        .forEach((element) => allcomponent.add(Components.fromMap(element)));
    return allcomponent;
  }
}
