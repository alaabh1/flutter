import 'package:flutter/material.dart';
import 'package:flutter_project/dashbord.dart';
import 'package:flutter_project/v2/model/component.model.dart';
import 'package:flutter_project/v2/model/family.model.dart';
import 'package:flutter_project/v2/screen/family/show.family.dart';
import 'package:flutter_project/v2/service/component.service.dart';
import 'package:intl/intl.dart';
import 'package:flutter_project/v2/service/family.service.dart';
import 'package:get/get.dart';

class AddComponentPage extends StatefulWidget {
  AddComponentPage({Key? key}) : super(key: key);

  @override
  _AddComponentState createState() => _AddComponentState();
}

class _AddComponentState extends State<AddComponentPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController quantityController = TextEditingController();
  ComponentService myservice = ComponentService();
  FamilyService familyService = FamilyService();

  bool _validate = false;
  bool _validatep = false;
  bool _validateu = false;
  bool _validatepa = false;
  String? value;
  String? familyname;
  DateTime? dateacq;

  static Future<DateTime?> dateDialog(BuildContext context) {
    return showDialog<DateTime>(
      context: context,
      builder: (BuildContext context) {
        return DatePickerDialog(
          initialDate: DateTime.now(),
          firstDate: DateTime(2000),
          lastDate: DateTime.now(),
          confirmText: "Procced",
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: FutureBuilder<List<Family>>(
          future: familyService.loadAllfamilies(),
          builder:
              (BuildContext context, AsyncSnapshot<List<Family>> snapshot) {
            Widget children;
            if (snapshot.hasData) {
              children = DropdownButton<String>(
                value: familyname,
                icon: const Icon(Icons.arrow_downward),
                iconSize: 24,
                hint: const Text("CHOOSE FAMILY"),
                elevation: 16,
                style: const TextStyle(color: Colors.deepPurple),
                underline: Container(
                  height: 2,
                  color: Colors.deepPurpleAccent,
                ),
                items: snapshot.data!.map<DropdownMenuItem<String>>((e) {
                  return DropdownMenuItem<String>(
                    alignment: AlignmentDirectional.center,
                    value: e.id.toString(),
                    child: Text(e.name),
                  );
                }).toList(),
                onChanged: (value) {
                  print("FAMILY NAME = $familyname");
                  setState(() {
                    familyname = value;
                  });
                },
              );
            } else {
              children = const Text('No FAMILY');
            }
            return Center(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(
                      height: 120,
                    ),
                    SizedBox(
                      width: 400,
                      child: TextField(
                        controller: nameController,
                        decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            labelText: 'component Name',
                            errorText:
                                _validate ? 'Value Can\'t Be Empty' : null),
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    SizedBox(
                      width: 400,
                      child: TextField(
                        controller: quantityController,
                        decoration: InputDecoration(
                            border: const OutlineInputBorder(),
                            labelText: 'quantity',
                            errorText:
                                _validate ? 'Value Can\'t Be Empty' : null),
                      ),
                    ),
                    ElevatedButton(
                        onPressed: () async {
                          DateTime? date = await dateDialog(context);
                          setState(() {
                            dateacq = date;
                          });
                        },
                        child: const Text("Date d'acquition")),
                    children,
                    const SizedBox(
                      height: 50,
                    ),
                    ElevatedButton(
                        onPressed: () async {
                          await myservice.addComponent(Component(
                              name: nameController.text,
                              addedAt: DateFormat('yyyy-MM-dd ')
                                  .format(dateacq!)
                                  .toString(),
                              quantity: int.parse(quantityController.text),
                              family: int.parse(familyname!)));
                        },
                        child: const Text("Confirm")),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
