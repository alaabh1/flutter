import 'package:flutter/material.dart';
import 'package:flutter_project/v2/model/family.model.dart';
import 'package:flutter_project/v2/screen/family/add.family.dart';
import 'package:flutter_project/v2/service/family.service.dart';

import 'package:get/get.dart';

class FamiliesListPage extends StatefulWidget {
  FamiliesListPage({Key? key}) : super(key: key);

  @override
  _FamiliesListState createState() => _FamiliesListState();
}

class _FamiliesListState extends State<FamiliesListPage> {
  FamilyService listfamilies = FamilyService();
  AddFamilyPage addfamily = AddFamilyPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     floatingActionButton: FloatingActionButton(
                      backgroundColor: const Color(0xff03dac6),
                      foregroundColor: Colors.black,
                      onPressed: () {
                        Get.to(addfamily);
                      },
                      child: Icon(Icons.add),
                    ),
      body: Container(
        child: FutureBuilder<List<Family>>(
          future: listfamilies.loadAllfamilies(),
          builder: (BuildContext context, AsyncSnapshot<List<Family>> snapshot) {
            if (!snapshot.hasData) {
              return Text("NO DATA");
            }
            return ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: <Widget>[
                      Card(
                        elevation: 8,
                        margin: const EdgeInsets.all(20),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(
                              leading: const Icon(Icons.album, size: 48),
                              title: Text(snapshot.data![index].name,
                                  style: const TextStyle(
                                      fontSize: 18, fontWeight: FontWeight.w600)),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                });
          },
        ),
        
      ),
     
    );
  }
}
