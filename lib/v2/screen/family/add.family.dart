import 'package:flutter/material.dart';
import 'package:flutter_project/dashbord.dart';
import 'package:flutter_project/v2/model/family.model.dart';
import 'package:flutter_project/v2/screen/family/show.family.dart';

import 'package:flutter_project/v2/service/family.service.dart';
import 'package:get/get.dart';

class AddFamilyPage extends StatefulWidget {
  AddFamilyPage({Key? key}) : super(key: key);

  @override
  _AddFamilyState createState() => _AddFamilyState();
}

class _AddFamilyState extends State<AddFamilyPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  FamilyService myservice = FamilyService();
  bool _validate = false;
  bool _validatep = false;
  bool _validateu = false;
  bool _validatepa = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
            child: Center(
              child: Column(
                children: [
                  SizedBox(
                    width: 400,
                    child: TextField(
                      controller: nameController,
                      decoration: InputDecoration(
                          border: const OutlineInputBorder(),
                          labelText: 'Family Name',
                          errorText:
                              _validate ? 'Value Can\'t Be Empty' : null),
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  ElevatedButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                    ),
                    child: const Text('Add'),
                    onPressed: () async {
                      nameController.text.isEmpty
                          ? _validate = true
                          : _validate = false;
                      await myservice
                          .addFamily(Family(name: nameController.text));
                      setState(() {});
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
