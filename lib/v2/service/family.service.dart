import 'package:flutter_project/v2/db/dbcreation.dart';
import 'package:flutter_project/v2/model/family.model.dart';
import 'package:flutter_project/v2/utils/constants.dart';
import 'package:sqflite/sqflite.dart';

class FamilyService {
  Future<int> addFamily(Family family) async {
    var db = await Stockdatabase.getDatabase();
    return db.insert(
      FAMILY_TABLE,
      family.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Family>> loadAllfamilies() async {
    var db = await Stockdatabase.getDatabase();
    List<Map<String, Object?>> familiesList = await db.query(FAMILY_TABLE);
    List<Family> list = [];
    for (var element in familiesList) {
      print(element.values);
      list.add(Family.fromMap(element));
    }
    return list;
  }
}
