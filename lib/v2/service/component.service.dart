import 'package:flutter_project/v2/db/dbcreation.dart';
import 'package:flutter_project/v2/model/component.model.dart';
import 'package:flutter_project/v2/utils/constants.dart';
import 'package:sqflite/sqflite.dart';

class ComponentService {
  Future<List<Component>> loadAllComponenets() async {
    var db = await Stockdatabase.getDatabase();
    List<Map<String, Object?>> componentList = await db.query(COMPONENT_TABLE);
    List<Component> list = [];
    for (var element in componentList) {
      list.add(Component.fromMap(element));
    }
    return list;
  }

  Future<List<Component>> searchComponenetByName(String name) async {
    var db = await Stockdatabase.getDatabase();
    List<Map<String, Object?>> componentList =
        await db.query(COMPONENT_TABLE, where: "name = ?", whereArgs: [name]);
    List<Component> list = [];
    for (var element in componentList) {
      list.add(Component.fromMap(element));
    }
    return list;
  }

  Future<Component> searchComponenetById(int id) async {
    var db = await Stockdatabase.getDatabase();
    List<Map<String, Object?>> componentList =
        await db.query(COMPONENT_TABLE, where: "id = ?", whereArgs: [id]);
    return Component.fromMap(componentList.first);
  }

  Future<int> addComponent(Component component) async {
    var db = await Stockdatabase.getDatabase();
    return db.insert(
      COMPONENT_TABLE,
      component.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<int> updateQuantity(int id, int quantity) async {
    var db = await Stockdatabase.getDatabase();
    return await db.update(
        COMPONENT_TABLE,
        {
          'quantity': quantity,
        },
        where: "id = ?",
        whereArgs: [id]);
  }
}
