import 'package:flutter_project/v2/db/dbcreation.dart';
import 'package:flutter_project/v2/model/loaned_object.model.dart';
import 'package:flutter_project/v2/model/loans.model.dart';
import 'package:flutter_project/v2/service/component.service.dart';
import 'package:flutter_project/v2/utils/constants.dart';
import 'package:sqflite/sqflite.dart';

class LoanService {
  //add

  Future<int> addLoan(Loan loan) async {
    var db = await Stockdatabase.getDatabase();
    var component =
        await ComponentService().searchComponenetById(loan.componentId);
    var newQuantity = component.quantity - loan.quantity;
    if (newQuantity < 0) {
      return -1;
    }
    ComponentService().updateQuantity(loan.componentId, newQuantity);
    return db.insert(
      LOAN_TABLE,
      loan.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  //update when returned

  Future<int> updateLoanState(
      int loanId, int componentId, int quantity, String state) async {
    var db = await Stockdatabase.getDatabase();
    var component = await ComponentService().searchComponenetById(componentId);
    var newQuantity = component.quantity + quantity;
    ComponentService().updateQuantity(componentId, newQuantity);

    return await db.update(
        LOAN_TABLE,
        {
          'state': state,
          'isReturned': true,
          'returnedAt': DateTime.now().toString()
        },
        where: "id = ?",
        whereArgs: [loanId]);
  }

  Future<List<Loan>> loadAllLoans() async {
    var db = await Stockdatabase.getDatabase();
    List<Map<String, Object?>> loansList =
        await db.query(LOAN_TABLE, where: "isReturned = ?", whereArgs: [false]);
    List<Loan> list = [];
    for (var element in loansList) {
      list.add(Loan.fromMap(element));
    }
    return list;
  }

  Future<List<LoanedObject>> loadAllLoansDetails() async {
    var db = await Stockdatabase.getDatabase();
    List<Map<String, Object?>> loansList = await db.rawQuery(
        "SELECT loan.id, member.firstName, member.lastName, member.firstNumber, loan.quantity, loan.componentId, member.secondNumber , component.name FROM loan , member, component WHERE loan.member =  member.id AND loan.componentId = component.id AND loan.isReturned = false ");
    List<LoanedObject> list = [];
    for (var element in loansList) {
      list.add(LoanedObject.fromMap(element));
    }
    return list;
  }
}
