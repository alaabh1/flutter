class Member {
  int id;
  final String firstName;
  final String lastName;
  final int firstNumber;
  final int secondNumber;

  Member({
    this.id = 0,
    required this.firstName,
    required this.lastName,
    required this.firstNumber,
    required this.secondNumber,
  });

  Map<String, dynamic> toMap() {
    return {
      'firstName': firstName,
      'lastName': lastName,
      'firstNumber': firstNumber,
      'secondNumber': secondNumber,
    };
  }

  static Member fromMap(Map<String, dynamic> json) {
    return Member(
      id: json['id'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      firstNumber: json['firstNumber'],
      secondNumber: json['secondNumber'],
    );
  }
}
