import 'package:flutter/material.dart';
import 'package:flutter_project/back/familyservice.dart';
import 'package:flutter_project/models/componentsmodel.dart';
import 'package:flutter_project/models/familymodel.dart';
import 'package:get/get.dart';

import 'back/componentsservice.dart';

class ManageComponentsPage extends StatefulWidget {
  ManageComponentsPage({Key? key}) : super(key: key);

  @override
  _ManageFamilyState createState() => _ManageFamilyState();
}

class _ManageFamilyState extends State<ManageComponentsPage> {
  final itemss = [""];

  TextEditingController componentnameController = TextEditingController();
  TextEditingController quantiteController = TextEditingController();
  Componentsservice myservice = Componentsservice();
  bool _validate = false;
  bool _validatep = false;
  bool _validateu = false;
  bool _validatepa = false;
  String? value;
  String? familyname;
  DateTime? dateacq;

  static Future<DateTime?> dateDialog(BuildContext context) {
    return showDialog<DateTime>(
      context: context,
      builder: (BuildContext context) {
        return DatePickerDialog(
          initialDate: DateTime.now(),
          firstDate: DateTime(2000),
          lastDate: DateTime.now(),
          confirmText: "Procced",
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<List<Family>>(
        future: Familyservice.getAllFamily(),
        builder: (BuildContext context, AsyncSnapshot<List<Family>> snapshot) {
          Widget children;
          if (snapshot.hasData) {
            children = DropdownButton<String>(
              value: familyname,
              icon: const Icon(Icons.arrow_downward),
              iconSize: 24,
              hint: const Text("CHOOSE FAMILY"),
              elevation: 16,
              style: const TextStyle(color: Colors.deepPurple),
              underline: Container(
                height: 2,
                color: Colors.deepPurpleAccent,
              ),
              items: snapshot.data!.map<DropdownMenuItem<String>>((e) {
                return DropdownMenuItem<String>(
                  alignment: AlignmentDirectional.center,
                  value: e.namef,
                  child: Text(e.namef!),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  familyname = value;
                });
              },
            );
          } else {
            children = const Text('No FAMILY');
          }
          return Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                const SizedBox(
                  height: 120,
                ),
                SizedBox(
                  width: 400,
                  child: TextField(
                    controller: componentnameController,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        labelText: 'component Name',
                        errorText: _validate ? 'Value Can\'t Be Empty' : null),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                SizedBox(
                  width: 400,
                  child: TextField(
                    controller: quantiteController,
                    decoration: InputDecoration(
                        border: const OutlineInputBorder(),
                        labelText: 'quantité',
                        errorText: _validate ? 'Value Can\'t Be Empty' : null),
                  ),
                ),
                ElevatedButton(
                    onPressed: () async {
                      DateTime? date = await dateDialog(context);
                      setState(() {
                        dateacq = date;
                      });
                    },
                    child: const Text("Date d'acquition")),
                children,
                const SizedBox(
                  height: 80,
                ),
                ElevatedButton(
                    onPressed: () async {
                      await myservice.insertcomponent(Components(
                          namec: componentnameController.text,
                          dateacq: dateacq.toString(),
                          quantite: int.parse(quantiteController.text),
                          namefa: familyname.toString()
                          ));
                    },
                    child: const Text("Confirm")),
              ],
            ),
          );
        },
      ),
    );
  }
}
