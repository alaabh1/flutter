class Components {
  int? id;
  final String namec;
  final String dateacq;
  final int quantite;
  final String namefa;

  Components({
    this.id,
    required this.namec,
    required this.dateacq,
    required this.quantite,
    required this.namefa,
  });
  Map<String, dynamic> toMap() {
    return {
      'namec': namec,
      'dateacq': dateacq,
      'quantite': quantite,
      'namefa': namefa,
    };
  }

  static Components fromMap(Map<String, dynamic> json) {
    return Components(
        namec: json['namec'],
        dateacq: json['dateacq'],
        quantite: json['quantite'],
        namefa: json['namefa']);
  }
}
